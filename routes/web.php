<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var $router Router
 */

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'users'], function () use ($router) {
    $router->post('register', 'UsersController@register');
    $router->post('login', 'UsersController@login');
});

$router->group(['prefix' => 'tickets', 'middleware' => 'auth'], function () use ($router) {
    $router->post('/', 'TicketsController@create');
    $router->put('/', 'TicketsController@update');

    $router->group(['prefix' => 'styles'], function () use ($router) {
        $router->get('/', 'TicketStylesController@all');
    });
});
