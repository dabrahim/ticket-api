<?php


namespace App\TicketAPI\Utils;


use Illuminate\Http\Response;

class ApiResponseManager {

    public static function response($response, int $status = 200) {
        return response()->json($response)
            ->setStatusCode($status);
    }

    private static function buildErrorResponse($statusCode, $errorName, $message, array $messages = []) {
        $errorResponse = [
            'status' => $statusCode,
            'error' => $errorName,
            'message' => $message
        ];

        if (!empty($messages)) {
            $errorResponse['messages'] = $messages;
        }

        return self::response($errorResponse, $statusCode);
    }

    public static function errorResponse($statusCode, $message, $messages = []) {
        return self::buildErrorResponse($statusCode, Response::$statusTexts[$statusCode], $message, $messages);
    }

    public static function basicResponse($message, array $payload = []) {
        $response = [
            'message' => $message
        ];

        $response = array_merge($response, $payload);

        return self::response($response);
    }

    public static function genericErrorResponse() {
        return self::errorResponse(Response::HTTP_INTERNAL_SERVER_ERROR,
            "Something went wrong, please retry later.");
    }
}
