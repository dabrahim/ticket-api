<?php

namespace App\TicketAPI\Utils;

use Jose\Component\Checker\AlgorithmChecker;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\Algorithm\HS256;
use Jose\Component\Signature\JWS;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSTokenSupport;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;

class JwtManager {
    private static $SECRET_KEY = null;
    private static $JWS_BUILDER = null;

    private static final function getSecretKey() {
        if (self::$SECRET_KEY == null) {
            self::$SECRET_KEY = JWKFactory::createFromSecret(
                'dzI6nbW4OcNF-AtfxGAmuyz7IpHRudBI0WgGjZWgaRJt6prBn3DARXgUR8NVwKhfL43QBIU2Un3AvCGCHRgY4TbEqhOi8-i98xxmCggNjde4oaW6wkJ2NgM3Ss9SOX9zS3lcVzdCMdum-RwVJ301kbin4UtGztuzJBeg5oVN00MGxjC2xWwyI0tgXVs-zJs5WlafCuGfX1HrVkIf5bvpE0MQCSjdJpSeVao6-RSTYDajZf7T88a2eVjeW31mMAg-jzAWfUrii61T_bYPJFOXW8kkRWoa1InLRdG6bKB9wQs9-VdXZP60Q4Yuj_WZ-lO7qV9AEFrUkkjpaDgZT86w2g',       // The shared secret
                [
                    'alg' => 'HS256',
                    'use' => 'sig'
                ]
            );
        }
        return self::$SECRET_KEY;
    }

    private static final function getJWSBuilder() {
        if (self::$JWS_BUILDER == null) {
            $algorithmManager = new AlgorithmManager([
                new HS256(),
            ]);
            self::$JWS_BUILDER = new JWSBuilder($algorithmManager);
        }
        return self::$JWS_BUILDER;
    }

    public static function buildToken(int $id) {
        $jwk = self::getSecretKey();

        // We get an instance of our JWS Builder.
        $jwsBuilder = self::getJWSBuilder();

        // The payload we want to sign. The payload MUST be a string hence we use our JSON Converter.
        $payload = json_encode([
            'id' => $id,
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + 3600 * 24 * 30,
            'iss' => $_ENV['APP_NAME'],
            'aud' => $_ENV['APP_NAME']
        ]);

        $header = [
            'alg' => 'HS256',
            'typ' => 'JWT'
        ];

        $jws = $jwsBuilder
            ->create()                               // We want to create a new JWS
            ->withPayload($payload)                  // We set the payload
            ->addSignature($jwk, $header) // We add a signature with a simple protected header
            ->build();                               // We build it

        $serializer = new CompactSerializer(); // The serializer

        return $serializer->serialize($jws, 0); // We serialize the signature at index 0 (we only have one signature).
    }

    public static function isValid($token) {
        $algorithmManager = new AlgorithmManager([
            new HS256(),
        ]);

        // We instantiate our JWS Verifier.
        $jwsVerifier = new JWSVerifier(
            $algorithmManager
        );

        $jws = self::unSerialize($token);

        // We verify the signature. This method does NOT check the header.
        // The arguments are:
        // - The JWS object,
        // - The key,
        // - The index of the signature to check.
        $isVerified = $jwsVerifier->verifyWithKey($jws, self::getSecretKey(), 0);

        $headerCheckerManager = new HeaderCheckerManager(
            [
                new AlgorithmChecker(['HS256']), // We check the header "alg" (algorithm)
                new TypeChecker(['JWT'])
            ],
            [
                new JWSTokenSupport(), // Adds JWS token type support
            ]
        );

        try {
            $headerCheckerManager->check($jws, 0, ['alg', 'typ']);
            $isHeaderOk = true;

        } catch (\Jose\Component\Checker\MissingMandatoryHeaderParameterException | \Jose\Component\Checker\InvalidHeaderException $e) {
            // TODO: lOG this exception
            //$headerOk = $e->getMessage();
            $isHeaderOk = false;
        }

        return $isVerified && $isHeaderOk;
    }

    /**
     * @param $token
     * @return JWS
     */
    private static function unSerialize($token): JWS {
        // The serializer manager. We only use the JWS Compact Serialization Mode.
        $serializerManager = new JWSSerializerManager([
            new CompactSerializer(),
        ]);

        // We try to load the token.
        return $serializerManager->unserialize($token);
    }

    public static function getPayload($token) {
        $jws = self::unserialize($token);
        return json_decode($jws->getPayload(), true);
    }
}
