<?php

namespace App\TicketAPI\Utils;

use Jose\Component\Checker\HeaderChecker;
use Jose\Component\Checker\InvalidHeaderException;

class TypeChecker implements HeaderChecker {

    private const HEADER_NAME = 'typ';

    /**
     * @var bool
     */
    private $protectedHeader = false;

    /**
     * @var string[]
     */
    private $supportedAlgorithms;

    /**
     * @param string[] $supportedAlgorithms
     * @param bool $protectedHeader
     */
    public function __construct(array $supportedAlgorithms, bool $protectedHeader = false) {
        $this->supportedAlgorithms = $supportedAlgorithms;
        $this->protectedHeader = $protectedHeader;
    }

    /**
     * {@inheritdoc}
     */
    public function checkHeader($value): void {
        if (!\is_string($value)) {
            throw new InvalidHeaderException('"typ" must be a string.', self::HEADER_NAME, $value);
        }
        if (!\in_array($value, $this->supportedAlgorithms, true)) {
            throw new InvalidHeaderException('Unsupported type.', self::HEADER_NAME, $value);
        }
    }

    public function supportedHeader(): string {
        return self::HEADER_NAME;
    }

    public function protectedHeaderOnly(): bool {
        return $this->protectedHeader;
    }

}
