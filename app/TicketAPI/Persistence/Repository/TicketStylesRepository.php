<?php


namespace App\TicketAPI\Persistence\Repository;


use App\TicketAPI\Persistence\DAO\TicketStylesDAO;

class TicketStylesRepository extends BaseRepository implements TicketStylesDAO {

    function getAll() {
        return $this->_db->runQuery("EXEC GetStyles_SP", [], false);
    }

}
