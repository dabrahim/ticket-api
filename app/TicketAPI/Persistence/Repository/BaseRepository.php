<?php


namespace App\TicketAPI\Persistence\Repository;


use App\TicketAPI\Persistence\PDOWrapper;

abstract class BaseRepository {
    /**
     * @var PDOWrapper
     */
    protected $_db;

    const FAILURE_RESULT = -1;

    public function __construct(PDOWrapper $pdo = null) {
        $this->_db = $pdo == null ? PDOWrapper::getInstance() : $pdo;
    }
}
