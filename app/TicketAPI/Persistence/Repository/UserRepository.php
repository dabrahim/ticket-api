<?php


namespace App\TicketAPI\Persistence\Repository;


use App\TicketAPI\Persistence\DAO\UserDAO;
use App\TicketAPI\Persistence\Model\User;

class UserRepository extends BaseRepository implements UserDAO {

    function create(User $user): int {
        $userId = self::FAILURE_RESULT;

        if ($this->_db->runStatement(
            "INSERT INTO users(first_name, last_name, password, username)
                        VALUES (:firstName, :lastName, :password, :username)",
            array(
                $user->getFirstName(),
                $user->getLastName(),
                password_hash($user->getPassword(), PASSWORD_DEFAULT),
                $user->getUserName()
            ))) {

            $userId = $this->_db->lastInsertId();
        }

        return $userId;
    }

    function hasValidCredentials(string $username, string $password): bool {
        $user = $this->_db->runQuery(
            "SELECT * FROM users WHERE username = :username", array($username)
        );

        return $user && password_verify($password, $user['password']);
    }

    function findById(int $id) {
        return $this->_db->runQuery(
            "SELECT * FROM users WHERE id = :id", array($id)
        );
    }

    function findByUsername(string $username) {
        return $this->_db->runQuery(
            "SELECT * FROM users WHERE username = :username", array($username)
        );
    }
}
