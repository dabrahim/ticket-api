<?php


namespace App\TicketAPI\Persistence\Repository;


use App\TicketAPI\Persistence\DAO\TicketDAO;

class TicketRepository extends BaseRepository implements TicketDAO {

    function create($strUser, $style) {
        $ticketId = self::FAILURE_RESULT;

        if ($this->_db->runStatement("EXEC CreateTicket_SP @TicketIssuer = :strUser, @TicketStyle = :style",
            array($strUser, $style))) {

            $ticketId = $this->_db->lastInsertId();
        }

        return $ticketId;
    }

    function update($strTicketID, $style) {
        return $this->_db->runStatement("EXEC UpdateTicket_SP @TicketID = :TicketID, @TicketStyle = :style",
            array($strTicketID, $style));
    }

}
