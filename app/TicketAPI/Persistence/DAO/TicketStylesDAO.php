<?php


namespace App\TicketAPI\Persistence\DAO;


interface TicketStylesDAO {
    function getAll();
}
