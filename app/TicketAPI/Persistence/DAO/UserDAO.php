<?php


namespace App\TicketAPI\Persistence\DAO;


use App\TicketAPI\Persistence\Model\User;

interface UserDAO {

    function create(User $user): int;

    function hasValidCredentials(string $username, string $password): bool;

    function findById(int $id);

    function findByUsername(string $username);
}
