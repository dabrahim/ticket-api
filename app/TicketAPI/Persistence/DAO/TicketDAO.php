<?php


namespace App\TicketAPI\Persistence\DAO;


interface TicketDAO {

    function create($strUser, $style);

    function update($strTicketID, $style);
}
