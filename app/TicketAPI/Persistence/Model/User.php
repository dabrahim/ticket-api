<?php


namespace App\TicketAPI\Persistence\Model;


class User {

    private $_id;
    private $_firstName;
    private $_lastName;
    private $_userName;
    private $_password;

    /**
     * User constructor.
     * @param $_firstName
     * @param $_lastName
     * @param $_userName
     * @param $_password
     */
    public function __construct($_firstName = null, $_lastName = null, $_userName = null, $_password = null) {
        $this->_firstName = $_firstName;
        $this->_lastName = $_lastName;
        $this->_userName = $_userName;
        $this->_password = $_password;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->_id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getFirstName() {
        return $this->_firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName): void {
        $this->_firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName() {
        return $this->_lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName): void {
        $this->_lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getUserName() {
        return $this->_userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName): void {
        $this->_userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword() {
        return $this->_password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void {
        $this->_password = $password;
    }

}
