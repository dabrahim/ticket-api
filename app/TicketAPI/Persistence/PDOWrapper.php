<?php


namespace App\TicketAPI\Persistence;


use PDO;
use PDOStatement;

class PDOWrapper extends PDO {
    /**
     * @var PDOWrapper
     */
    private static $_instance;

    /**
     * @return PDOWrapper
     */
    public static function getInstance() {
        if (self::$_instance == null) {
            self::$_instance = new PDOWrapper($_ENV['DB_CONNECTION'] . ":Server=" . $_ENV['DB_HOST'] . ";Database=" . $_ENV['DB_DATABASE'], $_ENV['DB_USERNAME'], $_ENV['DB_PASSWORD']);
            self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$_instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        }
        return self::$_instance;
    }

    public function runQuery($query, array $params = null, bool $singleFetch = true, bool $autoBind = true, $fetchMode = PDO::FETCH_ASSOC) {
        $pdoStatement = $this->setupPDOStatement($query, $params, $autoBind);
        $pdoStatement->execute();
        return $singleFetch ? $pdoStatement->fetch($fetchMode) : $pdoStatement->fetchAll($fetchMode);
    }

    /**
     * @param $statement
     * @param array|null $values
     * @param bool $autoBinding
     * @return bool
     * @throws PDOWrapperException
     */
    public function runStatement($statement, array $values = null, $autoBinding = true) {
        $pdoStatement = $this->setupPDOStatement($statement, $values, $autoBinding);
        return $pdoStatement->execute();
    }

    /**
     * @param $statement
     * @param $values
     * @param $autoBind
     * @return bool|PDOStatement
     */
    private function setupPDOStatement($statement, $values, $autoBind) {
        if ($autoBind) {
            $args = $this->getParamBindings($statement, $values);
        } else {
            $args = $values;
        }

        $pdoStatement = self::$_instance->prepare($statement);
        $this->bindValues($pdoStatement, $args);

        return $pdoStatement;
    }

    /**
     * @param $statement
     * @param $values
     * @return array
     * @throws PDOWrapperException
     */
    private function getParamBindings($statement, array $values = null) {
        preg_match_all("#:([^:,() ]+)#", $statement, $matches);

        $params = $matches[0];
        $paramsCount = sizeof($params);

        //If named params where found
        if ($paramsCount > 0) {

            if ($paramsCount == sizeof($values)) {
                $boundParams = array();

                foreach ($params as $index => $param) {
                    $boundParams[$param] = $values[$index];
                }
                return $boundParams;

            } else {
                throw new PDOWrapperException("The named parameters count doesn't match the values count.");
            }


        } else {
            //If no named params where found but the user provided arguments
            if ($values != null && sizeof($values) > 0) {
                throw new PDOWrapperException("No named parameters where found but values array found. Couldn't do the binding.");

                //No named parameters found and the values provided, that's fine. There's no bindings
            } else {
                return [];
            }
        }
    }

    /**
     * @param PDOStatement $pdoStatement
     * @param $values
     * @return PDOStatement
     */
    private function bindValues(PDOStatement $pdoStatement, array $values) {
        if ($values == null || sizeof($values) == 0) return $pdoStatement;

        foreach ($values as $key => $val) {
            if (!preg_match('/[:]+/', $key)) {
                $key = ":$key";
            }

            if (preg_match('/^[0-9]+(\.[0-9]+)?$/', $val)) { //is numeric with no power
                if (preg_match('/^[0-9]+$/', $val)) { //is int
                    $pdoStatement->bindValue($key, $val, PDO::PARAM_INT);

                } else /*if (preg_match('/^[0-9]+\.[0-9]+$/', $val)) */ {
                    //$pdoStatement->bindValue(':'.$key, $val);
                    $pdoStatement->bindValue($key, $val);
                }
            } else {
                $pdoStatement->bindValue($key, $val);
            }
        }
        return $pdoStatement;
    }

}
