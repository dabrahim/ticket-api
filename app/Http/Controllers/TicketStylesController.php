<?php


namespace App\Http\Controllers;


use App\TicketAPI\Persistence\Repository\TicketStylesRepository;
use App\TicketAPI\Utils\ApiResponseManager;

class TicketStylesController extends Controller {

    public function all(TicketStylesRepository $repository) {
        return ApiResponseManager::response($repository->getAll());
    }
}
