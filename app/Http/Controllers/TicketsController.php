<?php


namespace App\Http\Controllers;


use App\TicketAPI\Persistence\Repository\TicketRepository;
use App\TicketAPI\Utils\ApiResponseManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class TicketsController extends Controller {

    public function create(Request $request, TicketRepository $repository) {
        try {
            $this->validate($request, [
                'strUser' => 'required',
                'style' => 'required'
            ]);

            $strUser = $request->input('strUser');
            $style = $request->input('style');

            $ticketId = $repository->create($strUser, $style);

            if ($ticketId != TicketRepository::FAILURE_RESULT) {

                return ApiResponseManager::response([[
                    'TicketID' => $ticketId
                ]]);

            } else {
                return ApiResponseManager::genericErrorResponse();
            }

        } catch (ValidationException $e) {
            return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                "Invalid or missing params", $e->errors());
        }
    }

    public function update(Request $request, TicketRepository $repository) {
        try {
            $this->validate($request, [
                'strTicketID' => 'required',
                'style' => 'required'
            ]);

            $strTicketID = $request->input('strTicketID');
            $style = $request->input('style');

            $result = $repository->update($strTicketID, $style);

            return ApiResponseManager::response([[
                'Outcome' => $result ? 1 : 0
            ]]);

        } catch (ValidationException $e) {
            return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                "Invalid or missing params", $e->errors());
        }
    }
}
