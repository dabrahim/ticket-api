<?php


namespace App\Http\Controllers;


use App\TicketAPI\Persistence\Model\User;
use App\TicketAPI\Persistence\Repository\UserRepository;
use App\TicketAPI\Utils\ApiResponseManager;
use App\TicketAPI\Utils\JwtManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller {

    public function register(Request $request, UserRepository $repository) {
        try {
            $this->validate($request, [
                'firstName' => 'required',
                'lastName' => ['required'],
                'username' => 'required',
                'password' => 'required|min:8'
            ]);

            $firstName = $request->input('firstName');
            $lastName = $request->input('lastName');
            $username = $request->input('username');
            $password = $request->input('password');

            if ($repository->findByUsername($username) != false) {
                return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                    "This username is already used.");
            }

            $user = new User($firstName, $lastName, $username, $password);
            $userId = $repository->create($user);

            if ($userId != UserRepository::FAILURE_RESULT) {
                $result = $repository->findById($userId);

                return ApiResponseManager::basicResponse('Registration successful',
                    ['user' => $result]);

            } else {
                return ApiResponseManager::errorResponse(Response::HTTP_INTERNAL_SERVER_ERROR,
                    "Something went wrong, please retry later.");
            }

        } catch (ValidationException $e) {
            return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                "Invalid or missing params", $e->errors());
        }
    }

    public function login(Request $request, UserRepository $userRepository) {
        try {
            $this->validate($request, [
                'username' => 'required',
                'password' => 'required'
            ]);

            $username = $request->input('username');
            $password = $request->input('password');

            if ($userRepository->hasValidCredentials($username, $password)) {
                $user = $userRepository->findByUsername($username);

                return ApiResponseManager::basicResponse('Login successful', [
                    'jwt' => JwtManager::buildToken($user['id']),
                    'user' => $user
                ]);

            } else {
                return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                    "Login or password incorrect");
            }

        } catch (ValidationException $e) {
            return ApiResponseManager::errorResponse(Response::HTTP_BAD_REQUEST,
                "Invalid or missing credentials", $e->errors());
        }
    }

}
