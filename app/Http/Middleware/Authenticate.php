<?php

namespace App\Http\Middleware;

use App\TicketAPI\Utils\ApiResponseManager;
use App\TicketAPI\Utils\JwtManager;
use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Authenticate {
    /**
     * The authentication guard factory instance.
     *
     * @var Auth
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param Auth $auth
     * @return void
     */
    public function __construct(Auth $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

        if ($request->hasHeader('Authorization')
            && preg_match('#^[bB]earer (.*)$#', $request->header('Authorization'), $matches)) {

            $token = $matches[1];

            if (!JwtManager::isValid($token)) {
                return ApiResponseManager::errorResponse(Response::HTTP_UNAUTHORIZED,
                    'Invalid token');
            }

        } else {
            return ApiResponseManager::errorResponse(Response::HTTP_UNAUTHORIZED,
                'Missing token or Authorization header');
        }

        return $next($request);
    }
}
